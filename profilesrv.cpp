#include "profilesrv.h"
#include "files.h"
#include <windows.h>
#include <fstream>
#include <list>

using namespace std;

namespace Acad {

    ProfileSrv::ProfileSrv()
    {

    }

    ProfileSrv::~ProfileSrv()
    {

    }

    bool ProfileSrv::RestoreFixedProfile(wstring fileName){

        bool result = false;
        wstring bakFileName = fileName + L"_bak";
        ifstream file(bakFileName);
        if (file){
            file.close();
            CopyFile(bakFileName.c_str(), fileName.c_str(), FALSE);
            result = true;
        };

        return result;
    }

    bool ProfileSrv::CreateFixedProfileBackup(wstring fileName){

        bool result = true;
        wstring bakFileName = fileName + L"_bak";
        ifstream file(bakFileName);
        if (!file){
            CopyFile(fileName.c_str(), bakFileName.c_str(), TRUE);
            ifstream file(bakFileName);
            if (!file){
                result = false;
            }
        }else{
            file.close();
        }

        return result;
    }

    void ProfileSrv::DeleteFixedProfileBackup(wstring fileName){

        wstring bakFileName = fileName + L"_bak";
        ifstream file(bakFileName);
        if (file){
            file.close();
            DeleteFile(bakFileName.c_str());
        }
    }

    bool ProfileSrv::ChangeFixedProfile(wstring fileName, wstring changeFileNameBase, wstring localId){
        // changeFileName - 2 input!!!

//        wchar_t path[MAX_PATH];
//        GetCurrentDirectory(sizeof(path), path);
//        wstring changeFileName = path;
//        changeFileName = changeFileNameBase + L"\\MiniTools\\Support\\FixedProfile_" + localId;
        wstring changeFileName = changeFileNameBase + localId;

        list<wstring> changeList = Files::Files::GetFileLinesList(changeFileName);
        list<wstring> sourceList = Files::Files::GetFileLinesList(fileName);
        list<wstring> resultList;
        wstring str = L"<Profile>";
        while(!sourceList.empty()){
            wstring strTmp = sourceList.front();
            sourceList.pop_front();
            if(strTmp.find(str) != std::string::npos){
                resultList.push_back(str);
                strTmp = strTmp.substr(9);
                while(!changeList.empty()){
                    resultList.push_back(changeList.front());
                    changeList.pop_front();
                }
            }
            resultList.push_back(strTmp);
        }

        bool result = Files::Files::CreateTextFile(fileName, resultList);

        return result;
    }


    bool ProfileSrv::CheckBackup(wstring fileName){

        bool result = false;
        wstring bakFileName = fileName + L"_bak";
        ifstream file(bakFileName);
        if (file){
            file.close();
            result = true;
        }

        return result;
    }

}
