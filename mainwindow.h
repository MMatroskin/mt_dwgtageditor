#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QStringListModel>
#include <QString>
#include <QList>
#include <QDir>
#include <utility>
#include <vector>
#include <list>
#include "Reg.h"
#include "Acad.h"
#include "thread.h"

namespace Ui {

struct State{
    int boxAcad;
    int boxProfile;
};

struct ACDParamsState{
    bool isSecureModeChanged;
    bool isFixedProfileChanged;
    bool isSysvarsChanged;
};

class MainWindow;

}

class MainWindow :  public QMainWindow, public Ui_MainWindow
{
    Q_OBJECT

public:
    Reg::RegFunc *registry;
    vector<Acad::AcadApp>  acadList;
    map<wstring, wstring> params;
    wstring path;
    wstring appPath;
    bool isCalling;
    bool isSilent;

    explicit MainWindow(QWidget *parent = 0, bool isCalling = false);
    ~MainWindow();

    void SetInitalState();

    signals:
        void workCanceled();

protected:
    virtual void keyPressEvent(QKeyEvent *event);

    public slots:
        void SetValue(int value);
        void AppStop(int value);
        void AppReport(int value);
        void ItemReport(int value);

    private slots:
        void on_fileList_activated();
        void on_pushButtonFile_clicked();
        void on_pushButtonOk_clicked();
        void pushButtonOkEnabled();
        void SlotExit();
        void SlotRemoveAllRecords();
        void SlotRemoveRecords();
        void SlotCustomMenuRequested(QPoint pos);
        void on_actionAbout_triggered();
        void on_actionAboutQT_triggered();
        void on_actionHistory_triggered();
        void on_comboBoxAcadList_activated(int index);
//        void on_currentTabChanged(int index);

private:
    Ui::MainWindow *ui;
    QStringListModel *modelFileList;
    QStringListModel *modelAcadList;
    QStringListModel *modelProfilesList;
    QStringList fileList;
    QStringList fileSelectedList;
    wstring lastDwgPath;
    wstring currentFile;
    wstring taskLogFile;
    wstring scriptName;
    Acad::AcadApp *acd;
    Ui::Thread *threadObject;
    Ui::State comboBoxState;
    Ui::ACDParamsState paramsState;
    vector<pair<wstring, wstring>> currentProfilePropertyesList;
    vector<pair<wstring, bool>> reportList;
    map<wstring, wstring> paramsACD;
    int count;
    bool appRuning;
    bool appEnable;
    bool scriptSucsess;
    bool saveEmptyValues;
    bool isAcadLspRenamed;

    QStringList GetProfilesList(Acad::AcadApp  *app);
    void SetModelFileListOnStart(bool checkSubdirs);
    void SetAcadListData();
    void SetFileListData();
    void SetComboBoxAcadListData();
    void SetModelFileList(QStringList newFileList, bool save);
    bool CreateACDScript(QString name);
    void SetACDParams(wstring profile,
                      vector<pair<wstring, unsigned long>> sysvarsList,
                      vector<pair<wstring, wstring>> sysvarsStrList,
                      bool isBefore);
    void UpdateTagsInACD(QString profile,
                         QString scriptName);
    void StartACDInThread(QStringList fileNameList,
                            QString scriptFileName,
                            QString profile,
                            QString path,
                            QString text,
                            int timeOut,
                            bool silent);
    void StartAppInThread(QString app,
                            QString paramsStr,
                            int timeOut,
                            bool silent);
    bool CheckAndSetParams(map<wstring, wstring> paramsOld, map<wstring, wstring> paramsNew);
    bool CompareState(Ui::State state1, Ui::State state2);
    void ReportError();

};

#endif // MAINWINDOW_H
