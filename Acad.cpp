#include "Acad.h"
#include "Reg.h"

using namespace std;

namespace Acad{

    AcadApp::AcadApp(){
    }

    AcadApp::AcadApp(wstring acadKeyPath){
        SetRegKeyPath(acadKeyPath);
        SetProductName();
        SetRelease();
        SetAcadLocation();
        SetProductId();
        SetLocaleId();
        SetAcadProfiles();
    }

    AcadApp::~AcadApp(){
    }

    // getters

    wstring AcadApp::GetProductName(){

        return productName;
    }
    wstring  AcadApp::GetRelease(){

        return release;
    }

    wstring  AcadApp::GetAcadLocation(){

        return acadLocation;
    }

    wstring  AcadApp::GetProductId(){

        return productId;
    }

    wstring  AcadApp::GetLocaleId(){

        return localeId;
    }

    vector<wstring> AcadApp::GetPlotters(wstring profile){

        vector<wstring> result;
        wstring key = L"PrinterConfigDir";
        wstring currentPath = GetRegKeyPath() + L"\\Profiles\\" + profile + L"\\General";
        wstring parametr = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, currentPath, key);
        if (!parametr.empty()){
            wstring sMask = parametr + L"\\" + L"*.pc3";
            wstring wsMask(sMask.begin(), sMask.end());
            WIN32_FIND_DATA findFileData;
            HANDLE hFile;
            hFile = FindFirstFile(wsMask.c_str(), &findFileData);
            if(hFile != INVALID_HANDLE_VALUE){
                do{
                    wstring wsFileName = findFileData.cFileName;
                    string sFileName(wsFileName.begin(), wsFileName.end()); // del
                    result.push_back(wsFileName);
                }while(FindNextFile(hFile, &findFileData) != 0);
                FindClose(hFile);
            }
        }

        return result;
}

    vector<wstring> AcadApp::GetAcadProfiles(){

        return acadProfiles;
    }

    wstring  AcadApp::GetRegKeyPath(){

        return regKeyPath;
    }

    vector<pair<wstring, wstring>> AcadApp::GetProfileProperties(wstring profile, vector<wstring> keyList){

        vector<pair<wstring, wstring>> result;
        wstring currentPath = GetRegKeyPath() + L"\\Profiles\\" + profile + L"\\General";
        size_t keyListSize = keyList.size();
        for (size_t i = 0; i < keyListSize; i++){
            pair <wstring, wstring> key;
            wstring parametr = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, currentPath, keyList[i]);
            if (!parametr.empty()){
                key.first = keyList[i];
                key.second = parametr;
                result.push_back(key);
            }

        }
        if (result.size() != keyList.size()){
            result.clear();
        }
        return result;
    }

    vector<pair<wstring, unsigned long>> AcadApp::GetProfileSysvars(wstring profile, vector<pair<wstring, unsigned long>> keyList){

        vector<pair<wstring, unsigned long>> result;

        wstring currentPath = GetRegKeyPath() + L"\\Profiles\\" + profile + L"\\General";
        size_t keyListSize = keyList.size();
        for (size_t i = 0; i < keyListSize; i++){
            pair <wstring, unsigned long> key;
            unsigned long value = Reg::RegFunc::GetDwParametr(HKEY_CURRENT_USER, currentPath, keyList[i].first);
            key.first = keyList[i].first;
            if(value != UINT_MAX){
                key.second = value;
            }
            else {
                key.second = keyList[i].second;
            }
            result.push_back(key);
        }

        return result;
    }

    // setters

    void AcadApp::SetProductName(){

        wstring currentPath = GetRegKeyPath();
        productName = Reg::RegFunc::GetParametr(HKEY_LOCAL_MACHINE, currentPath, L"ProductName");
    }

    void AcadApp::SetRelease(){

        wstring currentPath = GetRegKeyPath();
        release = Reg::RegFunc::GetParametr(HKEY_LOCAL_MACHINE, currentPath, L"Release");
    }

    void AcadApp::SetAcadLocation(){

        wstring currentPath = GetRegKeyPath();
        acadLocation = Reg::RegFunc::GetParametr(HKEY_LOCAL_MACHINE, currentPath, L"AcadLocation");
    }

    void AcadApp::SetProductId(){
        wstring currentPath = GetRegKeyPath();
        productId = Reg::RegFunc::GetParametr(HKEY_LOCAL_MACHINE, currentPath, L"ProductId");
    }

    void AcadApp::SetLocaleId(){

        wstring currentPath = GetRegKeyPath();
        localeId = Reg::RegFunc::GetParametr(HKEY_LOCAL_MACHINE, currentPath, L"localeId");
    }

    void AcadApp::SetRegKeyPath(wstring str){

        regKeyPath = str;
    }

    void AcadApp::SetAcadProfiles(){

        wstring profilesPathTmp = GetRegKeyPath() + L"\\Profiles";
        wstring profilesPath(profilesPathTmp.begin(), profilesPathTmp.end()); // del
        acadProfiles = Reg::RegFunc::GetSubKeys(HKEY_CURRENT_USER, profilesPath);
    }

    bool AcadApp::SetProfileSysvars(wstring profile, vector<pair<wstring, unsigned long>> keyList){

        bool result = true;

        wstring currentPath = GetRegKeyPath() + L"\\Profiles\\" + profile + L"\\General";
        size_t keyListSize = keyList.size();
        for (size_t i = 0; i < keyListSize; i++){
            bool resultTmp = Reg::RegFunc::SetParametr(HKEY_CURRENT_USER, currentPath, keyList[i].first, keyList[i].second);
            if (resultTmp == false){
                result = false;
            }
        }

        return result;
    }

    bool AcadApp::SetProfileSysvars(wstring profile, vector<pair<wstring, wstring>> keyList){

        bool result = true;

        wstring currentPath = GetRegKeyPath() + L"\\Profiles\\" + profile + L"\\General";
        size_t keyListSize = keyList.size();
        for (size_t i = 0; i < keyListSize; i++){
            bool resultTmp = Reg::RegFunc::SetParametr(HKEY_CURRENT_USER, currentPath, keyList[i].first, keyList[i].second);
            if (resultTmp == false){
                result = false;
            }
        }

        return result;
    }

    // other

    bool AcadApp::IsSecureModeOn(wstring profile){
        bool result = false;
        wstring str = this->release.substr(0,4);
        double tmp = stod(str, 0);
        if (tmp >= 19.1){
            wstring currentPath = GetRegKeyPath() + L"\\Profiles\\" + profile + L"\\Variables";
            wstring name = L"SECURELOAD";
            wstring value = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, currentPath, name);
            if((value != L"0") || (value == L"")) {
                result = true;
            }
        }

        return result;
    }

    bool AcadApp::SecureModeChange(wstring profile){
        bool result = false;
        wstring str = this->release.substr(0,4);
        if (stod(str, 0) >= 19.1){
            wstring currentPath = GetRegKeyPath() + L"\\Profiles\\" + profile + L"\\Variables";
            wstring name = L"SECURELOAD";
            wstring value = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, currentPath, name);
            if (value == L"0"){
                value = L"1";
            }
            else{
                value = L"0";
            }
            result = Reg::RegFunc::SetParametr(HKEY_CURRENT_USER, currentPath, name, value);
        }

        return result;
    }

}
