#include "configSrv.h"
#include "files.h"
#include <locale>
#include <windows.h>
#include <algorithm>

using namespace std;

namespace Files {

    ConfigSrv::ConfigSrv(){
    }

    ConfigSrv::~ConfigSrv(){
    }

    map<wstring, wstring> ConfigSrv::GetSectParams(wstring iniFileName, wstring sectionName){

        map<wstring, wstring> result;
        wstring sep = L"=";
        wstring sepCom = L";";
        wstring head = L"[" + sectionName + L"]";
        list<wstring> lineList = Files::GetFileLinesList(iniFileName);
        if(!lineList.empty()){
            list<wstring>::iterator it;
            it = find(lineList.begin(), lineList.end(), head);
            if(it != lineList.end()){
                it++;
                while (*it != L""){
                    wstring line = *it;
                    size_t posCom = line.find(sepCom);
                    if(posCom != std::string::npos){
                        line = line.substr(0, posCom);
                    }
                    if(!line.empty()){
                        size_t posSep = line.find(sep);
                        wstring key = line.substr(0 , posSep);
                        wstring value = line.substr(posSep + 1);
                        result.insert(pair<wstring, wstring>(key, value));
                    }
                    it++;
                }
            }
        }

        return result;
    }

    vector<pair<wstring, wstring>> ConfigSrv::GetSectParamsVec(wstring iniFileName, wstring sectionName){

        vector<pair<wstring, wstring>> result;
        wstring sep = L"=";
        wstring sepCom = L";";
        wstring head = L"[" + sectionName + L"]";
        list<wstring> lineList = Files::GetFileLinesList(iniFileName);
        if(!lineList.empty()){
            list<wstring>::iterator it;
            it = find(lineList.begin(), lineList.end(), head);
            if(it != lineList.end()){
                it++;
                while (*it != L""){
                    wstring line = *it;
                    size_t posCom = line.find(sepCom);
                    if(posCom != std::string::npos){
                        line = line.substr(0, posCom);
                    }
                    if(!line.empty()){
                        size_t posSep = line.find(sep);
                        wstring key = line.substr(0 , posSep);
                        wstring value = line.substr(posSep + 1);
                        result.push_back(pair<wstring, wstring>(key, value));
                    }
                    it++;
                }
            }
        }

        return result;
    }

    list<wstring> ConfigSrv::InsertParamsInSect(list<wstring> sourceList, map<wstring,wstring> paramsMap, wstring sectionName){

        list<wstring> targetList;
        wstring head = L"[" + sectionName + L"]";
        wstring sep = L"";
        auto it = sourceList.begin();
        while (*it != head){
            targetList.push_back(*it);
            it++;
        }
        targetList.push_back(head);
        auto itMap = paramsMap.begin();
        while(itMap != paramsMap.end()){
            wstring tmpStr = itMap->first + L"=" + itMap->second;
            targetList.push_back(tmpStr);
            itMap++;
        }
        while (*it != sep){
            it++;
        }
        while (it != sourceList.end()){
            targetList.push_back(*it);
            it++;
        }
        while(targetList.back() == L""){
            targetList.pop_back();
        } // clear end of file

        return targetList;
    }

}
