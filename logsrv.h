#ifndef LOGSRV_H
#define LOGSRV_H

#include<string>

using namespace std;

namespace Files {

    class LogSrv
    {
    public:
        LogSrv();
        ~LogSrv();

        static wstring GetSysvarValue(string value);
        static wstring GetLogFileName(wstring appName);
        static wstring GetLineUserDate();
    };

}


#endif // LOGSRV_H
