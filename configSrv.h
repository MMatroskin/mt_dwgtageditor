#ifndef CONFIGSRV_H
#define CONFIGSRV_H

#include <map>
#include <string>
#include <list>
#include <vector>

using namespace std;

namespace Files
{
    class ConfigSrv{
    public:
        ConfigSrv();
        ~ConfigSrv();

        static std::map<wstring, wstring> GetSectParams(wstring iniFileName, wstring sectionName);
        static std::vector<pair<wstring, wstring>> GetSectParamsVec(wstring iniFileName, wstring sectionName);
        static std::list<wstring> InsertParamsInSect(list<wstring> sourceList,
                                                      map<wstring,wstring> params,
                                                      wstring sectionName);

    };
}

#endif // CONFIGSRV_H
