#-------------------------------------------------
#
# Project created by QtCreator 2019-05-14T16:28:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MT_DwgTagEditor
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    dialoghistory.cpp \
    logsrv.cpp \
        main.cpp \
        mainwindow.cpp \
    appsrv.cpp \
    configSrv.cpp \
    files.cpp \
    Acad.cpp \
    acadsrv.cpp \
    Reg.cpp \
    thread.cpp \
    filesACD.cpp \
    profilesrv.cpp \
    countwindows.cpp \
    filesQt.cpp

HEADERS += \
    dialoghistory.h \
    logsrv.h \
        mainwindow.h \
    appsrv.h \
    configSrv.h \
    files.h \
    Acad.h \
    acadsrv.h \
    Reg.h \
    thread.h \
    filesACD.h \
    profilesrv.h \
    countwindows.h \
    filesQt.h

FORMS += \
        dialoghistory.ui \
        mainwindow.ui
