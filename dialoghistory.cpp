#include "dialoghistory.h"
#include "ui_dialoghistory.h"

DialogHistory::DialogHistory(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogHistory)
{

    ui->setupUi(this);
}

DialogHistory::~DialogHistory()
{
    delete ui;
}

void DialogHistory::ShowData(QString data){

    ui->textBrowserHistory->setText(data);
}
