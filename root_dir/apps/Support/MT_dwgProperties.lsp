;; Copyright (c) Maks Tulin 2018
;; functions for work thith drawing properties
;;

(vl-load-com)

(defun MT-getPropertyValue (key / propertyList result item)
;; get drawing propertie value
;; call: (MT-getPropertyValue key)
;; returned walue: string (value) or Nil

	(setq result nil)
	(setq propertyList (MT-getAllDwgPropertiesList))
	(if (setq item (assoc key propertyList))
		(setq result (cdr item))
	)
	(princ)
	result
)

(defun MT-getAllDwgPropertiesList( / acadObj doc summaryInfo result item key value)
;; get all drawing properties
;; call: (MT-getAllDwgPropertiesList)
;; returned walue: list of pair(key.value) 

	(setq acadObj (vlax-get-acad-object))
	(setq doc (vla-get-ActiveDocument acadObj))
	(setq summaryInfo (vla-get-SummaryInfo doc))
	
	;; standard properties
	(setq result (list
		(cons "Author"  (vla-get-Author summaryInfo))
		(cons "Comments"  (vla-get-Comments summaryInfo))
		(cons "HyperlinkBase"  (vla-get-HyperlinkBase summaryInfo))
		(cons "Keywords"  (vla-get-Keywords summaryInfo))
		(cons "Subject"  (vla-get-Subject summaryInfo))
		(cons "Title"  (vla-get-Title summaryInfo))
	      )
    )
	
	;; custom properties
	(setq count (vla-NumCustomInfo summaryInfo))
	(while (> count 0)
		(setq count (1- count))
		(vla-GetCustomByIndex summaryInfo count 'key 'value)
		(setq item (cons key value))
		(setq result
			(append result (list item))
		)
	)
	
;	(vlax-release-object summaryInfo)
	result
)

(defun MT-getStdDwgPropertiesList( / acadObj doc summaryInfo result item)
;; get standard drawing properties
;; call: (MT-getStdDwgPropertiesList)
;; returned walue: list of pair(key.value) 

	(setq acadObj (vlax-get-acad-object))
	(setq doc (vla-get-ActiveDocument acadObj))
	(setq summaryInfo (vla-get-SummaryInfo doc))
	
	;; standard properties
	(setq result (list
		(cons "Author"  (vla-get-Author summaryInfo))
		(cons "Comments"  (vla-get-Comments summaryInfo))
		(cons "HyperlinkBase"  (vla-get-HyperlinkBase summaryInfo))
		(cons "Keywords"  (vla-get-Keywords summaryInfo))
		(cons "Subject"  (vla-get-Subject summaryInfo))
		(cons "Title"  (vla-get-Title summaryInfo))
	      )
    )
	
;	(vlax-release-object summaryInfo)
	result
)


;; (setq changeList (list (cons "Author"  "��������") (cons "Comments"  "") (cons "Title" "***")))
;; (("Author" . "��������") ("Comments" . "") ("Title" . "***"))

(defun MT-setStdDwgProperties (newPropertyList saveEmpty / acadObj doc summaryInfo propertyList item oldItem value oldValue)
;; set standrd drawing properties
;; call: (MT-setStdDwgProperties changeList) ; valueList: ((key.value)(key.value))
;; returned walue: nil

	(setq propertyList (MT-getAllDwgPropertiesList))
	(setq acadObj (vlax-get-acad-object))
	(setq doc (vla-get-ActiveDocument acadObj))
	(setq summaryInfo (vla-get-SummaryInfo doc))
	
	(if (setq item (assoc "Author" newPropertyList))
		(progn
			(setq oldItem (assoc "Author" propertyList))
			(setq oldValue (cdr oldItem))
			(setq value (cdr item))
			;(if (= value nil)
		  	;(princ value)
		  	(if (= value "")
				(if (or (= oldValue nil) (= oldValue "") (= saveEmpty nil))
					(vla-put-Author summaryInfo (chr 32))
				)
				(vla-put-Author summaryInfo value)
			)
		)
		
	)
	(if (setq item (assoc "Comments" newPropertyList))
		;(vla-put-Comments summaryInfo (cdr item))
	  	(progn
			(setq oldItem (assoc "Comments" propertyList))
			(setq oldValue (cdr oldItem))
			(setq value (cdr item))
			;(if (= value nil)
		  	;(princ value)
		  	(if (= value "")
				(if (or (= oldValue nil) (= oldValue "") (= saveEmpty nil))
					(vla-put-Comments summaryInfo (chr 32))
				)
				(vla-put-Comments summaryInfo value)
			)
		)
	  	
	)
	(if (setq item (assoc "HyperlinkBase" newPropertyList))
		;(vla-put-HyperlinkBase summaryInfo (cdr item))
		(progn
			(setq oldItem (assoc "HyperlinkBase" propertyList))
			(setq oldValue (cdr oldItem))
			(setq value (cdr item))
			;(if (= value nil)
		  	;(princ value)
		  	(if (= value "")
				(if (or (= oldValue nil) (= oldValue "") (= saveEmpty nil))
					(vla-put-HyperlinkBase summaryInfo (chr 32))
				)
				(vla-put-HyperlinkBase summaryInfo value)
			)
		)
	)
	(if (setq item (assoc "Keywords" newPropertyList))
		;(vla-put-Keywords summaryInfo (cdr item))
	  	(progn
			(setq oldItem (assoc "Keywords" propertyList))
			(setq oldValue (cdr oldItem))
			(setq value (cdr item))
		  	;(if (= value nil)
		  	;(princ value)
		  	(if (= value "")
				(if (or (= oldValue nil) (= oldValue "") (= saveEmpty nil))
					(vla-put-Keywords summaryInfo (chr 32))
				)
				(vla-put-Keywords summaryInfo value)
			)
		)
	)
	(if (setq item (assoc "Subject" newPropertyList))
		;(vla-put-Subject summaryInfo (cdr item))
		(progn
			(setq oldItem (assoc "Subject" propertyList))
			(setq oldValue (cdr oldItem))
			(setq value (cdr item))
			;(if (= value nil)
		  	;(princ value)
		  	(if (= value "")
				(if (or (= oldValue nil) (= oldValue "") (= saveEmpty nil))
					(vla-put-Subject summaryInfo (chr 32))
				)
				(vla-put-Subject summaryInfo value)
			)
		)
	)
	(if (setq item (assoc "Title" newPropertyList))
		;(vla-put-Title summaryInfo (cdr item))
		(progn
			(setq oldItem (assoc "Title" propertyList))
			(setq oldValue (cdr oldItem))
			(setq value (cdr item))
			;(if (= value nil)
		  	;(princ value)
		  	(if (= value "")
				(if (or (= oldValue nil) (= oldValue "") (= saveEmpty nil))
					(vla-put-Title summaryInfo (chr 32))
				)
				(vla-put-Title summaryInfo value)
				
			)
		)
	)
	
;	(vlax-release-object summaryInfo)
)

(defun MT-setCustomDwgPropertie(key value saveEmpty / acadObj doc summaryInfo propertyList item oldValue)
;; set custom drawing propertie
;; call: (MT-setCustomDwgPropertie "key" "Value" T) 
;; returned walue: nil

	(setq propertyList (MT-getAllDwgPropertiesList))
	(setq acadObj (vlax-get-acad-object))
	(setq doc (vla-get-ActiveDocument acadObj))
	(setq summaryInfo (vla-get-SummaryInfo doc))

	;; custom properties only:
	(setq item (assoc key propertyList))
	(if item
		(progn
			(setq oldValue (cdr item))
			(if (= value "")
				(if (or (= oldValue nil) (= oldValue "") (= saveEmpty nil))
					(vla-SetCustomByKey summaryInfo key (chr 32))
				)
				(vla-SetCustomByKey summaryInfo key value)
			)
		)
	  	(vla-AddCustomInfo summaryInfo key value); always !!
	)

;	(vlax-release-object summaryInfo)
)


;; (setq changeList (list (cons "����������"  "�������������0") (cons "����������"  "***") (cons "����������"  "��������00") (cons "Key"  "Val") (cons "KK"  "VV")))
;; (("����������" . "�������������0") ("����������" . "***") ("����������" . "��������00"))

(defun MT-setAllCustomDwgProperties (newPropertyList saveEmpty / result)
;; set custom drawing properties
;; call: (MT-setAllCustomDwgProperties changeList T) 
;; returned walue: nil

  	(princ)
	(mapcar
		(function 
			(lambda (x)
				(MT-setCustomDwgPropertie (car x) (cdr x) saveEmpty)
			)
		)
		newPropertyList
	)
  	(princ)
	(setq result nil)
)

(defun MT-setAllDwgProperties (propertyList saveEmpty / stdPropertiesNamesList stdList customList x)
;; set all drawing properties
;; call: (MT-setAllDwgProperties changeList nil) 
;; returned walue: nil

  	(setq stdPropertiesNamesList 
		(list
			"Author"
			"Comments"
			"Keywords"
			"Subject"
			"Title"
			"HyperlinkBase"
		)
	)
	
	(foreach x propertyList
		(if (vl-position (car x) stdPropertiesNamesList)
			(setq stdList (append stdList (list x)))
			(setq customList (append customList (list x)))
		)
	)

	(MT-setStdDwgProperties stdList saveEmpty)
	(MT-setAllCustomDwgProperties customList saveEmpty)
	
	(setq result nil)
)

