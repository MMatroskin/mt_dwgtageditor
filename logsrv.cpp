#include "logsrv.h"
#include <windows.h>
#include <iostream>
#include <ctime>

#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "User32.lib")

using namespace std;

namespace Files{

    LogSrv::LogSrv()
    {

    }

    LogSrv::~LogSrv()
    {

    }

    wstring LogSrv::GetLogFileName(wstring appName){

        wstring result = appName;
        string value = "COMPUTERNAME";
        string name = "USERNAME";
        wstring str = L" " + GetSysvarValue(value) + L" " + GetSysvarValue(name);
        result = result + str + L".log";

        return result;
    }

    wstring LogSrv::GetSysvarValue(string value){

        char *resultTmp = getenv(value.c_str());
        wchar_t buffer[8081];
        wstring result = L"Undefined";
        int nCnt = MultiByteToWideChar(CP_ACP, 0, resultTmp, -1, buffer, sizeof(buffer));
        if (nCnt > 0){
            result = buffer;
        }

        return result;
    }

    wstring LogSrv::GetLineUserDate(){

        string value = "USERNAME";
        wstring userName = GetSysvarValue(value);

        time_t rawtime;
        struct tm * timeinfo;
        time( &rawtime ); // time (seconds)
        timeinfo = localtime (&rawtime);

        int yearTmp = timeinfo->tm_year + 1900;
        int monTmp = timeinfo->tm_mon + 1;

        wstring dateTimeStr = L"";
        dateTimeStr = dateTimeStr
                + std::to_wstring(timeinfo->tm_mday) + L"."
                + std::to_wstring(monTmp) + L"."
                + std::to_wstring(yearTmp) + L" "
                + std::to_wstring(timeinfo->tm_hour) + L":"
                + std::to_wstring(timeinfo->tm_min) + L":"
                + std::to_wstring(timeinfo->tm_sec);

        wstring result = userName + L"; " + dateTimeStr + L":";

        return result;
    }
}
