#include "filesACD.h"
#include "files.h"
#include <fstream>
#include <string>
#include <locale>
#include <windows.h>
//#include <afx.h>
#include <cstdio>

using namespace std;

namespace Files {

    FilesACD::FilesACD(){
    }

    FilesACD::~FilesACD(){
    }

    bool FilesACD::RenameAcadLsp(wstring path, bool isBefore){

        wstring fileNameOld;
        wstring fileNameNew;
        if(isBefore){
            fileNameOld = path + L"\\acad.lsp";
            fileNameNew = path + L"\\acad.lsp_bak";
        }else{
            fileNameNew = path + L"\\acad.lsp";
            fileNameOld = path + L"\\acad.lsp_bak";
        }

        bool result = Files::RenameFile(fileNameOld, fileNameNew);

        return result;
    }

    bool FilesACD::CreateSetupScriptAcd(bool isSetup, wstring fileName, bool isSilent){

        list<wstring> lineList;
        wstring str = L"T";
        if(isSilent){
            str = L"nil";
        }
        wchar_t pathTmp[MAX_PATH];
        GetCurrentDirectory(sizeof(pathTmp), pathTmp);
        wstring path = pathTmp;
        path = Files::AddSlash(path);
        path = path + L"\\\\MiniTools";
        lineList.push_back(L"(setq path");
        lineList.push_back(L"\"" + path + L"\"");
        lineList.push_back(L")");
        lineList.push_back(L"(load (findfile (strcat path \"\\\\Main\\\\MT_setup.lsp\")))");
        if(isSetup){
            lineList.push_back(L"(MT-setup path " + str + L")");
//            lineList.push_back(L"(princ  " + str + L")");
        }else{
            lineList.push_back(L"(MT-remove path " + str + L")");
//            lineList.push_back(L"(princ  " + str + L")");

        }

        lineList.push_back(L"(setq path nil)");
        lineList.push_back(L"_quit");
        lineList.push_back(L"_y");

        bool result = Files::CreateTextFile(fileName, lineList);// create

        return result;
    }

    bool FilesACD::CreatePlottersDescScript(wstring name, wstring appPath){

        bool scriptSuccess = false;

        wstring pathACD = Files::AddSlash(appPath) + L"\\\\Support";

        list<wstring> lineList = {
            L"(setq path \"" + pathACD + L"\")",
            L"(setq file0 (findfile (strcat path \"\\\\\" \"MT_BatchPlotFiles.LSP\")))",
            L"(setq file1 (findfile (strcat path \"\\\\\" \"MT_BatchPlotPrinters.LSP\")))",
            L"(load file0)",
            L"(load file1)",
            L"(MT-exportPlottersPrefs path)",
            L"(setvar \"PROXYNOTICE\" 0)",
            L"_Quit",
            L"_No"
        };

        std::wstring scriptName = Files::ChangeSlash(name);
        scriptSuccess = Files::CreateTextFile(scriptName, lineList); // create Script

        return scriptSuccess;
    }

    bool FilesACD::CreatePlotScript(wstring scriptFileName,
                          wstring appPath,
                          wstring plotStyle,
                          int copies,
                          bool pdfPlot,
                          wstring outPdfPath){

        bool scriptSuccess = false;
        wstring pdfStr = L"0";
        if(pdfPlot){
            pdfStr = L"1";
        }
        wstring copiesStr = std::to_wstring(copies);
        wstring pathACD = Files::AddSlash(appPath) + L"\\\\Support";

        wstring pdfPath = L"nil";
        if(outPdfPath != L""){
            wstring outPath = Files::ChangeSlash(outPdfPath);
            WIN32_FIND_DATA fileData;
            HANDLE hFind;
            hFind = FindFirstFile(outPath.c_str(), &fileData);
            if(hFind != INVALID_HANDLE_VALUE){
                DWORD atr = GetFileAttributes(outPath.c_str());
                if (atr & FILE_ATTRIBUTE_DIRECTORY){
                    pdfPath = L"\"" + Files::AddSlash(outPath) + L"\"";
                }
            }
        }

        list<wstring> lineList = {
            L"(setq bp (getvar \"BACKGROUNDPLOT\"))",
            L"(setq po (getvar \"PLOTTRANSPARENCYOVERRIDE\"))",
            L"(setvar \"FILEDIA\" 0)",
            L"(setvar \"PROXYSHOW\" 1)",
            L"(setvar \"LAYEREVALCTL\" 0)",
            L"(setvar \"BACKGROUNDPLOT\" 0)",
            L"(setvar \"PLOTTRANSPARENCYOVERRIDE\" 2)",
            L"(setq plotTable \"" + plotStyle + L"\")",
            L"(setq copies " + copiesStr + L")",
            L"(setq pdfPlot " + pdfStr + L")",
            L"(setq outPath " + pdfPath + L")",
            L"(setq path \"" + pathACD + L"\")",
            L"(vl-load-com)",
            L"(setq file0 (findfile (strcat path \"\\\\\" \"MT_BatchPlotFiles.LSP\")))",
            L"(setq file1 (findfile (strcat path \"\\\\\" \"MT_BatchPlotPrinters.LSP\")))",
            L"(setq file2 (findfile (strcat path \"\\\\\" \"MT_BatchPlotPrint.LSP\")))",
            L"(load file0)",
            L"(load file1)",
            L"(load file2)",
            L"(MT-printFrames path plotTable copies pdfPlot outPath)",
            L"(setvar \"PLOTTRANSPARENCYOVERRIDE\" po)",
            L"(setvar \"BACKGROUNDPLOT\" bp)",
            L"(setvar \"FILEDIA\" 1)",
            L"(setvar \"PROXYNOTICE\" 0)",
            L"_Quit",
            L"_No"
        };

        std::wstring scriptName = Files::ChangeSlash(scriptFileName);
        scriptSuccess = Files::CreateTextFile(scriptName, lineList); // create Script

        return scriptSuccess;
    }

    bool FilesACD::CreateTagEditScript(wstring scriptName, wstring lspFileName, map<wstring, wstring> params, bool saveEmptyValues){
        // create ACD script file

        bool scriptSucsess = false;
        list<wstring> lineList0 = {
            L"(setq newPropertiesList",
            L"(list"
        };

        wstring  saveTags = L"nil";
        if(saveEmptyValues){
            saveTags = L"T";
        }

        list<wstring> paramsACD;
        map<wstring, wstring>::iterator it0;
        for(it0 = params.begin(); it0 != params.end(); it0++){
            if(it0->first != L"path"){
                wstring key = it0->first;
                wstring val = it0->second;
                wstring line = L"";
                line = L"(cons \"" + key + L"\" \"" + val + L"\")";
                lineList0.push_back(line);
            };
        }

        list<wstring> lineList1 = {
            L"))",
            L"(setq fileName0 \"" + lspFileName + L"\")",
            L"(findFile fileName0)",
            L"(load fileName0)",
            L"(MT-setAllDwgProperties newPropertiesList " + saveTags + L")",
            L"_Quit",
            L"_No"
        };

        list<wstring> lineList = lineList0;
        lineList.splice(lineList.end(), lineList1);

        scriptSucsess = Files::Files::CreateTextFile(scriptName, lineList); // create Script

        return scriptSucsess;
    }
}
