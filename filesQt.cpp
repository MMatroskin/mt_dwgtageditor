#include "filesQt.h"
#include <QDir>
#include <fstream>
#include <QList>
#include <QTextStream>
#include <QTextCodec>
#include <string>
#include <locale>
#include <windows.h>
#include <cstdio>
//#include <algorithm>

using namespace std;

namespace Files {

    FilesQt::FilesQt(){
    }

    FilesQt::~FilesQt(){
    }

    bool FilesQt::DeleteFileNow(QString fileName){

        bool result = true;
        if(remove(fileName.toStdString().c_str()) != 0){
            result = false;
        }

        return result;
    }

    QString FilesQt::ChangeSlash(QString stringIn){
        stringIn.replace("/", "\\");
        return stringIn;
    }

    QString FilesQt::AddQuotes(QString stringIn){

        QString result;
        QString sep = "\\";
        QStringList tmpList = stringIn.split(sep);
        size_t size = tmpList.size();
        for (size_t i = 1; i < size; i++){
            tmpList[i] = "\"" + tmpList[i] + "\"";
        }
        result = tmpList.join(sep);

        return result;
    }

    QString FilesQt::AddSlash(QString stringIn){

        QString result;
        QString sep = "\\";
        QStringList tmpList = stringIn.split(sep);
        QString newSep = "\\\\";
        result = tmpList.join(newSep);

        return result;
    }

}
