#ifndef FILES_H
#define FILES_H

#include <list>
#include <map>
#include <QSizeF>
#include <QString>
#include "mainwindow.h"

using namespace std;

namespace Files
{
    class FilesQt{
    public:
        FilesQt();
        ~FilesQt();

        static bool DeleteFileNow(QString fileName);
        static QString ChangeSlash(QString stringIn);
        static QString AddQuotes(QString stringIn);
        static QString AddSlash(QString stringIn);

    };
}

#endif // FILES_H
