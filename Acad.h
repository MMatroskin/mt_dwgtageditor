#ifndef ACAD_H
#define ACAD_H

#include <windows.h>
#include <vector>
#include <string>
#include <utility>

using namespace std;

namespace Acad
{
    class AcadApp
    {
    protected:
        wstring productName;
        wstring release;
        wstring acadLocation;
        wstring productId;
        wstring localeId;
        vector<wstring> acadProfiles;
        wstring regKeyPath;

    public:
        AcadApp();
        AcadApp(wstring acadKeyPath); // достаточно чтобы развернуть класс полностью
        ~AcadApp();

        wstring GetProductName();
        wstring GetRelease();
        wstring GetAcadLocation();
        wstring GetProductId();
        wstring GetLocaleId();
        wstring GetRegKeyPath();

        vector<wstring> GetAcadProfiles();

        vector<wstring> GetPlotters(wstring profile);

        vector<pair<wstring, wstring>> GetProfileProperties(
            wstring profile,
            vector<wstring> keyList);

        vector<pair<wstring, unsigned long>> GetProfileSysvars(
            wstring profile,
            vector<pair<wstring, unsigned long>> keyList);

        bool SetProfileSysvars(
            wstring profile,
            vector<pair<wstring, unsigned long>> keyList);

        bool SetProfileSysvars(
            wstring profile,
            vector<pair<wstring, wstring>> keyList);

        bool IsSecureModeOn(wstring profile);

        bool SecureModeChange(wstring profile);

    private:
        void SetProductName();
        void SetRelease();
        void SetAcadLocation();
        void SetProductId();
        void SetLocaleId();
        void SetRegKeyPath(wstring path);
        void SetAcadProfiles();

    };
}

#endif
