#include "mainwindow.h"
#include "acadsrv.h"
#include "configSrv.h"
#include "files.h"
#include <codecvt>
#include <locale>
#include <windows.h>
#include <QApplication>
#include <QMessageBox>
#include <QStyleFactory>

// ошибка линковки с2017 / c2019:
// QT необходимо указывать внешние либы, для msvs - #pragma comment
// для MinGW - LIBS += /путь_до_либы/libastral.a  в .pro-файле (стат.линк.)
// либо - LIBS += -L./путь_до_либы/ -lastral  в .pro-файле (дин.линк.)

#pragma comment(lib, "Kernel32.lib")
#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "User32.lib")

using namespace std;

int main(int argc, char *argv[]){
    // symbols - separatots in input string:
    // #    sect
    // $    space
    // @
    
    bool isCalling = false;
    if (argc > 1){
        isCalling = true;
    }

    int result = 0;

	// in rev2 - find and use ODA File Converter (if not found - ACAD)
	// 2 mainWin (or hide ACAD list and ACAD profiles list items in mainWin) - ??

    // get current path ()
    wstring currentPath;
    WCHAR buffer[MAX_PATH];
    DWORD r = GetModuleFileName(NULL, buffer, sizeof(buffer) / sizeof(buffer[0]));
    if(r > 0){
        currentPath = buffer;
        size_t p = currentPath.find_last_of(L"\\");
        currentPath = currentPath.substr(0, p);
    }else{
        wchar_t path[MAX_PATH];
        GetCurrentDirectory(sizeof(path), path);
        currentPath = path; // path of parent app !!
    }
	
//    wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter; // string <-> wstring converter
    vector<Acad::AcadApp> acadList = Acad::AcadSrv::GetInstalledAcad();
//    map<wstring, wstring> params;
    map<wstring, wstring> paramsACD;
    wstring workDir = L"";

    if(isCalling){

        // convert
        char* szMbStr = argv[1];        // src string
        wchar_t awcBuffer[8081];        // dst string

        // Windows Default Codepage -> UTF-16
        size_t nCnt = MultiByteToWideChar(CP_ACP, 0, szMbStr, -1, awcBuffer, sizeof(awcBuffer));
        if (nCnt == 0){
            QString message = "Invalid input!";
            QMessageBox msgBox;
            msgBox.setText(message);
            msgBox.setWindowTitle("Error!");

            msgBox.exec();

            exit(1);
        }
        wstring paramStr = awcBuffer;

//        string srcStr = argv[1];
//        wstring paramStr = converter.from_bytes(srcStr); // s->w
        
        wstring oldSep = L"$"; // spaces
        wstring newSep = L" ";

        if(!paramStr.empty()){
            paramStr = Files::Files::ChangeSubstr(paramStr, oldSep, newSep);
        };


//        wchar_t path[MAX_PATH];
//        GetCurrentDirectory(sizeof(path), path);
//        wstring iniFileName = path;
//        iniFileName = iniFileName + L"\\MT_Projects.ini"; // test _appdir=apps

        wstring rootDir = currentPath;
        size_t pos = rootDir.find_last_of(L"\\");
        rootDir = rootDir.substr(0 , pos);
        list<wstring> iniFiles = Files::Files::SearchFiles(rootDir, L"MT_Projects.ini", false);
        if(iniFiles.size() == 0){
            QString message = "Configuratin file not found!";
            QMessageBox msgBox;
            msgBox.setText(message);
            msgBox.setWindowTitle("Error!");

            msgBox.exec();

            exit(1);
        }

        wstring iniFileName = iniFiles.front();

        map<wstring, wstring> paramsDirs = Files::ConfigSrv::GetSectParams(iniFileName, L"partDirs");
        map<wstring, wstring> propertiesNames = Files::ConfigSrv::GetSectParams(iniFileName, L"dwgPropertiesNames");

        map<wstring, wstring> params;
        wstring sep0 = L"#"; // params
        wstring sep1 = L"="; // keys/values
        size_t prev = 0;
        size_t next;
        size_t delta = 1;
        size_t posCurrrent;
        wstring key;
        wstring value;
//        wstring tmp;
        bool cont;

        
        // get params from argv
        do{
            cont = false;
            wstring tmp = paramStr.substr(prev);
            if((next = paramStr.find(sep0, prev)) != wstring::npos ){
                tmp = paramStr.substr(prev, next - prev );
                cont = true;
            }
            if((posCurrrent = tmp.find(sep1, 0)) != wstring::npos ){
                key = tmp.substr(0, posCurrrent);
                value = tmp.substr(posCurrrent + 1);
                params.insert(pair<wstring, wstring>(key, value));
            }
            prev = next + delta;
        }while(cont);

        // set work dir
        wstring subdir;
        map<wstring, wstring>::iterator it0;
        it0 = paramsDirs.find(L"_drawingsdir");
        if(it0 != paramsDirs.end()){
            subdir = it0->second;
        }
        map<wstring, wstring>::iterator it1;
        it1 = params.find(L"path");
//        if(it1 != params.end()){
//            it1->second = it1->second + L"\\" + subdir; // ???
//        }

        // get work dir
        if(it1 != params.end()){
//            workDir = it1->second;
            workDir = it1->second;
            workDir = Files::Files::ChangeSubstr(workDir,L"\\\\",L"\\");
            workDir = workDir + L"\\" + subdir;
        }

        // set params for ACD:
        map<wstring, wstring>::iterator it2 = propertiesNames.begin();
        while(it2 != propertiesNames.end()){
            wstring str = it2->first;
            wstring k;
            wstring v;
            it1 = params.find(str);
            if(it1 != params.end()){
                v = it1->second;
            }else{
                v = L"";
            }
            k = it2->second;
            paramsACD.insert(pair<wstring, wstring>(k, v));
            it2++;
        }

        // delete emty items from acad list
        vector<Acad::AcadApp>::iterator i = acadList.begin();
        while(i != acadList.end()){
            wstring str = i->GetProductName();
            if(str == L""){
                acadList.erase(i);
                i = acadList.begin();
            }else{
                i++;
            }
        }
    }

    QApplication app(argc, argv);
    app.setStyle(QStyleFactory::create("Fusion"));

    if (acadList.empty()){

        QString message = "ACAD application not found!";
        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();

    }else{

        MainWindow w(0, isCalling);

        w.acadList = acadList;
        w.params = paramsACD;
        w.path = workDir;
        w.appPath = currentPath;
        w.isSilent = true; // in test - false, in prod - true

        w.SetInitalState();

        w.show();
        result = app.exec();
    }

    return result;
}
