#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "acadsrv.h"
#include "files.h"
#include "filesACD.h"
#include "profilesrv.h"
#include "logsrv.h"
#include "dialoghistory.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QTime>
#include <windows.h>
#include <cstdio>

#define TIME_OUT 180000// test - 180 sec !!


MainWindow::MainWindow(QWidget *parent, bool isCalling):
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->isCalling = isCalling;

    registry = new Reg::RegFunc();
    acd = new Acad::AcadApp();
    modelFileList = new QStringListModel(this);
    modelAcadList = new QStringListModel(this);
    modelProfilesList = new QStringListModel(this);

    ui->listView->setModel(modelFileList);
    ui->tabWidget->setCurrentWidget(ui->tabFiles);

    connect(ui->listView, &QListView::clicked, this, &MainWindow::on_fileList_activated);
    connect(ui->pushButtonCancel, &QPushButton::clicked, this, &MainWindow::SlotExit);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::SlotExit);

    ui->listView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->listView, &QListView::customContextMenuRequested, this, &MainWindow::SlotCustomMenuRequested);
//    connect(ui->tabWidget, &QTabWidget::currentChanged, this, &MainWindow::on_currentTabChanged);

    // inital state
    paramsState = {false, false, false};

}


MainWindow::~MainWindow(){

    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event){

    int key = event->key();
    if(key == Qt::Key_Delete){
        if(event->modifiers() & Qt::ShiftModifier){
            SlotRemoveAllRecords();
        }else {
            SlotRemoveRecords();
        }
    }else if(key == Qt::Key_Plus && event->modifiers() & Qt::CTRL){
        on_pushButtonFile_clicked();
    }

}

void MainWindow::SlotCustomMenuRequested(QPoint pos){

    QMenu * menu = new QMenu(this);
    QAction * addItems = new QAction("Добавить файлы (Ctrl+Plus)", this);
    QAction * removeItems = new QAction("Удалить из списка (Del)", this);
    QAction * removeAll = new QAction("Очистить список (Shift+Del)", this);

    connect(addItems, SIGNAL(triggered()), this, SLOT(on_pushButtonFile_clicked()));
    connect(removeItems, SIGNAL(triggered()), this, SLOT(SlotRemoveRecords()));
    connect(removeAll, SIGNAL(triggered()), this, SLOT(SlotRemoveAllRecords()));

    menu->addAction(addItems);
    menu->addAction(removeItems);
    menu->addAction(removeAll);

    menu->popup(ui->listView->viewport()->mapToGlobal(pos));
}

void MainWindow::SlotRemoveRecords(){

    for(int i = 0; i < fileSelectedList.size(); i++){
        int idx = fileList.indexOf(fileSelectedList[i]);
        if(idx >= 0 && idx < fileList.size()){
            fileList.removeAt(idx);
        }
    }
    fileSelectedList.clear();
    SetModelFileList(fileList, false);
}

void MainWindow::SlotRemoveAllRecords(){

    fileSelectedList.clear();
    fileList.clear();
    SetModelFileList(fileList, false);
}


void MainWindow::on_actionAbout_triggered(){

    QDate currentDate = QDate::currentDate();
    int currentYear = currentDate.year();
    int startYear = 2019;
    QString tmp = QString::number(startYear);
    if(currentYear > startYear){
        tmp = tmp + " - " + QString::number(currentYear);
    }
    QString str = "Batch tag editing utility for AutoCAD.\n(c) Maksim Tulin " + tmp + "\nnovymir-vlg@yandex.ru \nhttps://t.me/MMatroskin";
    QMessageBox::about(nullptr, "About", str);
}

void MainWindow::on_actionAboutQT_triggered(){

    QMessageBox::aboutQt(this, "About Qt");
}

void MainWindow::on_fileList_activated(){

    fileSelectedList.clear();
    QModelIndexList indexList = ui->listView->selectionModel()->selectedIndexes();
    for (int i = 0; i < indexList.count(); i++){
        int tmp = indexList.at(i).row();
        fileSelectedList.append(fileList[tmp]);
    }
}

void MainWindow::on_actionHistory_triggered(){

    QString msg;
    list<wstring> lineList = Files::Files::GetFileLinesList(taskLogFile);
    list<wstring>::iterator it;
    for(it = lineList.begin(); it != lineList.end(); it++){
        wstring tmp = it->data();
        msg = msg + "\n" + QString::fromStdWString(tmp);
    }

    DialogHistory *dlgHistory = new DialogHistory(this);


    // scroll area color - ???

    dlgHistory->setModal(true);
    dlgHistory->setAttribute(Qt::WA_DeleteOnClose);
    dlgHistory->setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    dlgHistory->setWindowTitle("История");
    dlgHistory->ShowData(msg);
    dlgHistory->show();
}

//void MainWindow::on_currentTabChanged(int index){

//    if(index == 1){
//        pushButtonOkEnabled();
//    }
//}

void MainWindow::pushButtonOkEnabled(){
    // enable OkButton if fileList not emty

    ui->pushButtonOk->setEnabled(modelFileList->rowCount() > 0);
}


void MainWindow::on_pushButtonFile_clicked(){

    QString workPath = QString::fromStdWString(lastDwgPath);
    if(workPath == ""){
        workPath = QDir::homePath();
    }

    QStringList inputFilesList =
            QFileDialog::getOpenFileNames(
                this,
                nullptr,
                workPath,
                tr("*.dwg"),
                nullptr,
                QFileDialog::ReadOnly |
                QFileDialog::DontUseCustomDirectoryIcons |
                QFileDialog::DontResolveSymlinks |
                QFileDialog::DontUseNativeDialog);

    if(inputFilesList.isEmpty() == false){
        QString tmp = inputFilesList[0];
        int index = tmp.lastIndexOf("/");
        tmp.truncate(index);
        lastDwgPath = tmp.toStdWString(); // Files::ChangeSlash(tmp.toStdWString()) - ???
        SetModelFileList(inputFilesList, true);
    }
}

void MainWindow::SetInitalState(){

    bool checkSubdirs = true;
    bool isDwgDirExist = Files::Files::CheckDir(path);
    if(!isDwgDirExist){
        string sysvarName = "USERPROFILE";
        this->path = Files::LogSrv::GetSysvarValue(sysvarName);
        checkSubdirs = false;
    }
    this->lastDwgPath = path;
    this->taskLogFile = appPath + L"\\" + Files::LogSrv::GetLogFileName(L"MT_tagedithistory");

    // read ini file (later):
    ui->lineEditCode->setText(QString::fromStdWString(this->params[L"Title"]));
    ui->lineEditStage->setText(QString::fromStdWString(this->params[L"Keywords"]));
    ui->lineEditReleaseDate->setText(QString::fromStdWString(this->params[L"_release_date"]));
    ui->lineEditAuthor0->setText(QString::fromStdWString(this->params[L"_author1"]));
    ui->lineEditAuthor1->setText(QString::fromStdWString(this->params[L"_author2"]));
    ui->lineEditCheck->setText(QString::fromStdWString(this->params[L"_inspector"]));
    ui->lineEditStd->setText(QString::fromStdWString(this->params[L"_standard_control"]));
    ui->lineEditHead->setText(QString::fromStdWString(this->params[L"_head_of_dept"]));
    ui->lineEditPm->setText(QString::fromStdWString(this->params[L"_project_manager"]));
    ui->lineEditFirm->setText(QString::fromStdWString(this->params[L"_company"])); // fix it later ???
    ui->textEditObject->setText(QString::fromStdWString(this->params[L"Subject"]));
    ui->textEditPlant->setText(QString::fromStdWString(this->params[L"Comments"]));

    SetAcadListData();
    SetModelFileListOnStart(checkSubdirs);

}

void MainWindow::SetModelFileList(QStringList newFileList, bool save){

    QStringList fileListTmp = newFileList;
    if(save){
        fileListTmp = fileList + fileListTmp;
    }
    this->fileList = fileListTmp;

    int dd = fileList.removeDuplicates();

    modelFileList->removeRows(0, modelFileList->rowCount());
    modelFileList->setStringList(fileList);

    ui->listView->setModel(modelFileList);
    ui->listView->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->listView->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    pushButtonOkEnabled();
}


void MainWindow::SetModelFileListOnStart(bool checkSubdirs){

    QStringList fileListStart;
    wstring dir;
    wstring mask = L"*.dwg";

    dir = path;
    if(dir != L""){
        std::list<std::wstring> fileListTmp = Files::Files::SearchFiles(dir, mask, checkSubdirs);
        for(list<wstring>::iterator it1 = fileListTmp.begin(); it1 != fileListTmp.end(); it1++){
            fileListStart.push_back(QString::fromStdWString(*it1));
        }
    }

    SetModelFileList(fileListStart, false);
}


void MainWindow::SetAcadListData(){

    QStringList acadNamesList;
    for(size_t i = 0; i < acadList.size(); i++){
        acadNamesList.push_back(QString::fromStdWString(acadList[i].GetProductName()));
    }
    modelAcadList->setStringList(acadNamesList);

    SetComboBoxAcadListData();
}

QStringList MainWindow::GetProfilesList(Acad::AcadApp  *app){

    QStringList acadProfilesList;
    vector<wstring> acadProfilesTmp = app->GetAcadProfiles();
    for(size_t i = 0; i < acadProfilesTmp.size(); i++){
        acadProfilesList.push_back(QString::fromStdWString(acadProfilesTmp[i]));
    }

    return acadProfilesList;
}

void MainWindow::SetComboBoxAcadListData(){

//    QStringList acadNamesList;
//    for(size_t i =0; i < acadList.size(); i++){
//        acadNamesList.push_back(QString::fromStdWString(acadList[i].GetProductName()));
//    }
//    modelAcadList->setStringList(acadNamesList);
    ui->comboBoxAcadList->setModel(modelAcadList);
    ui->comboBoxAcadList->setCurrentIndex(0);

    this->on_comboBoxAcadList_activated(0);

}

void MainWindow::on_comboBoxAcadList_activated(int index)
{
    acd = &acadList[index];
    QStringList acadProfilesList = GetProfilesList(acd);
    modelProfilesList->removeRows(0, modelProfilesList->rowCount());
    modelProfilesList->setStringList(acadProfilesList);
    ui->comboBoxAcadProfiles->setModel(modelProfilesList);
    ui->comboBoxAcadProfiles->setCurrentIndex(0);

//    this->on_comboBoxAcadProfiles_activated(0);
}


void MainWindow::SlotExit(){

    bool isScriptDeleting = Files::Files::DeleteFileNow(scriptName);// delete script
//    if (isScriptDeleting == false){
//        // dick with him
//    }
    if(isAcadLspRenamed){
        wstring lspPath;
        wstring key = L"ProfileStorage";
        wstring keyPath = acd->GetRegKeyPath() + L"\\FixedProfile\\General";
        wstring value = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, keyPath, key);
        wstring item = L"\\profiles\\fixedprofile.aws";

        size_t pos = value.find(item);
        if(pos !=  wstring::npos){
            lspPath =  value.substr(0, pos);
            this->isAcadLspRenamed = !Files::FilesACD::RenameAcadLsp(lspPath, false);
        }
    }
    this->close();
    // delete tmp files and close window
}

void MainWindow::on_pushButtonOk_clicked(){

    this->appEnable = false;
    this->isAcadLspRenamed = false;
    comboBoxState = {
        ui->comboBoxAcadList->currentIndex(),
        ui->comboBoxAcadProfiles->currentIndex()
    };

    this->saveEmptyValues = ui->checkBoxEmpty->isChecked();
    QString profile = ui->comboBoxAcadProfiles->currentText();

    map<wstring, wstring> oldParams = this->params;
    map<wstring, wstring> newParams;

    // read ini file (later):
    newParams[L"Title"] = ui->lineEditCode->text().toStdWString();
    newParams[L"Subject"] = ui->textEditObject->toPlainText().toStdWString();
    newParams[L"Comments"] = ui->textEditPlant->toPlainText().toStdWString();
    newParams[L"Keywords"] = ui->lineEditStage->text().toStdWString();
    newParams[L"_release_date"] = ui->lineEditReleaseDate->text().toStdWString();
    newParams[L"_project_manager"] = ui->lineEditPm->text().toStdWString();
    newParams[L"_standard_control"] = ui->lineEditStd->text().toStdWString();
    newParams[L"_head_of_dept"] = ui->lineEditHead->text().toStdWString();
    newParams[L"_inspector"] = ui->lineEditCheck->text().toStdWString();
    newParams[L"_author1"] = ui->lineEditAuthor0->text().toStdWString();
    newParams[L"_author2"] = ui->lineEditAuthor1->text().toStdWString();
    newParams[L"_company"] = ui->lineEditFirm->text().toStdWString();
    if(ui->checkBoxClearFirm->isChecked()){
        newParams[L"_company"] = L" ";
    }

    bool isParamsChanged = CheckAndSetParams(oldParams, newParams);
    if(isParamsChanged || scriptSucsess == false){
        QString scriptNameTmp = QDir::tempPath() + "/" + "dwgProps.scr"; // script in %tempdir%
        this->scriptName = Files::Files::ChangeSlash(scriptNameTmp.toStdWString());
        this->scriptSucsess = CreateACDScript(scriptNameTmp);
    }

    if (scriptSucsess){
        appEnable = true;
        vector<pair<wstring, unsigned long>> sysvarsDefaultList = {
            {L"ShowProxyGraphics", 1}, // PROXYSHOW
            {L"ShowProxyDialog", 1}, // PROXYNOTICE
            {L"ARXDemandLoad", 3} // DEMANDLOAD
        }; // ACAD defaults

        vector<pair<wstring, unsigned long>> sysvarsList = {
            {L"ShowProxyGraphics", 1},
            {L"ShowProxyDialog", 0},
            {L"ARXDemandLoad", 2} // 0 - ???
        };

        vector<pair<wstring, unsigned long>> sysvarsListBakup =
                acd->GetProfileSysvars(profile.toStdWString(), sysvarsDefaultList);

        vector<wstring> keyList = {
            L"ACAD"
        };
        this->currentProfilePropertyesList =
                    acd->GetProfileProperties(profile.toStdWString(), keyList);
        SetACDParams(profile.toStdWString(), sysvarsList, currentProfilePropertyesList, true);
        if (paramsState.isSysvarsChanged == false){
            QString message = "Системные переменные не измнены!";
            QMessageBox msgBox;
            msgBox.setText(message);
            msgBox.setWindowTitle("Error!");

            msgBox.exec();
        }else{
            list<wstring> logLineList;
            wstring logMsg = Files::LogSrv::GetLineUserDate();
            logLineList.push_back(logMsg);
            Files::Files::UpdateTextFile(taskLogFile, logLineList);
            UpdateTagsInACD(profile, QString::fromStdWString(scriptName));
        }
        SetACDParams(profile.toStdWString(), sysvarsListBakup, currentProfilePropertyesList, false);

    }else{
    QString str = "Script file not created!";
    QMessageBox::critical(
                0,
                "Error!",
                str,
                QMessageBox::Ok | QMessageBox::Escape
                );
    }

//    // close window - ???
//    if(isCalling){
//        SlotExit();
//    }
}

bool MainWindow::CreateACDScript(QString name){
    // create ACD script file

    wstring lspFileNameSrc = this->appPath;
    wstring lspFileName = Files::Files::AddSlash(lspFileNameSrc);
    lspFileName = lspFileName + L"\\\\Support\\\\MT_dwgProperties.lsp";
    std::wstring scriptName = Files::Files::ChangeSlash(name.toStdWString());

    return Files::FilesACD::CreateTagEditScript(scriptName,lspFileName, paramsACD, saveEmptyValues);
}

void MainWindow::SetACDParams(wstring profile,
                  vector<pair<wstring, unsigned long>> sysvarsList,
                  vector<pair<wstring, wstring>> sysvarsStrList,
                  bool isBefore){

    bool sysvarsStrSuccess = true;
    bool isSecureModeChanged = paramsState.isSecureModeChanged;
    bool isSysvarsChanged = paramsState.isSysvarsChanged;
    bool isFixedProfileChanged = paramsState.isFixedProfileChanged;

    wstring key = L"ProfileStorage";
    wstring keyPath = acd->GetRegKeyPath() + L"\\FixedProfile\\General";
    wstring value = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, keyPath, key);

    if (isBefore){
        // hide acad warnings (or restote defaults)
        wstring changeFileName = Files::Files::ChangeSubstr(this->appPath, L"\\\\", L"\\");
        changeFileName = changeFileName + L"\\Support\\FixedProfile_";

        bool backupExist = Acad::ProfileSrv::CheckBackup(value);
        bool isBackupCreated;

        if(backupExist == false){
            isBackupCreated = Acad::ProfileSrv::CreateFixedProfileBackup(value); // create if not exist
            isFixedProfileChanged = Acad::ProfileSrv::ChangeFixedProfile(value, changeFileName, acd->GetLocaleId());
            paramsState.isFixedProfileChanged = isFixedProfileChanged;
        }

        // SECURELOAD disable in ACAD 2014+
        if (acd->IsSecureModeOn(profile)){
            isSecureModeChanged = acd->SecureModeChange(profile);
            paramsState.isSecureModeChanged = isSecureModeChanged;
        }

    }else{
        // restore sysvar and profile
        if (isSecureModeChanged == true){
            bool res = acd->SecureModeChange(profile);
            if(res){
                isSecureModeChanged = false;
            }
        }
        paramsState.isSecureModeChanged = isSecureModeChanged;
        sysvarsStrSuccess = acd->SetProfileSysvars(profile, sysvarsStrList);
    }

    // setup sysvars
    bool sysvarsSuccess = acd->SetProfileSysvars(profile, sysvarsList); // проверить на кириллицу

    if(sysvarsSuccess && sysvarsStrSuccess){
        if(isSysvarsChanged == false ){
            isSysvarsChanged = true;
        }
        else {
            isSysvarsChanged = false;
        }
        paramsState.isSysvarsChanged = isSysvarsChanged;
    }
}

void MainWindow::UpdateTagsInACD(QString profile, QString scriptFileName){
    // main task

    QString strMsg = "";
    bool silent = this->isSilent;

    wstring lspPath = L"";
    wstring key = L"ProfileStorage";
    wstring keyPath = acd->GetRegKeyPath() + L"\\FixedProfile\\General";
    wstring value = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, keyPath, key);
    wstring item = L"\\profiles\\fixedprofile.aws";

    size_t pos = value.find(item);
    if(pos !=  wstring::npos){
        lspPath = value.substr(0, pos);
    }

//    if(lspPath !=  L""){
//        this->isAcadLspRenamed = Files::FilesACD::RenameAcadLsp(lspPath, true);
//    } // rename "acad.lsp" - delete if not required

    if (paramsState.isSysvarsChanged == false){
        QString message = "Системные переменные не измнены!";
        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();
    }else{
        QStringList fileNameList = modelFileList->stringList();
        QString supportPathApp = QString::fromStdWString(appPath) + "\\Support";

        if(appEnable){
            // prolonged operation. Runing the ACAD app & Lisp programm in this
            StartACDInThread(fileNameList,
                             scriptFileName,
                             profile,
                             supportPathApp,
                             strMsg,
                             TIME_OUT,
                             silent);

            // in rev2 - change ACAD to ODA File Converter (convert DWG-DXF -> edit -> convert DXF-DWG)
        }
    }
    if(isAcadLspRenamed && (lspPath !=  L"")){
        this->isAcadLspRenamed = !Files::FilesACD::RenameAcadLsp(lspPath, false);
    }
}

void MainWindow::StartACDInThread(QStringList fileNameList,
                                  QString scriptFileName,
                                  QString profile,
                                  QString path,
                                  QString text,
                                  int timeOut,
                                  bool silent){

    wstring wsAppName = Acad::AcadSrv::GetAcadFullName(acd);
    QString appName = QString::fromStdWString(wsAppName);

    QString supportPath;
    for(size_t i = 0; i < currentProfilePropertyesList.size(); i++){
        if(currentProfilePropertyesList[i].first == L"ACAD"){
            supportPath = QString::fromStdWString(currentProfilePropertyesList[i].second);
        }
    };
    if(!path.isEmpty()){
        supportPath = path + ";" + supportPath;
    }

    // progress bars color
    QPalette plt = this->palette();
    plt.setColor(QPalette::Highlight, QColor(77, 166, 255, 255));

    count = 0;

    threadObject = new Ui::Thread;
    threadObject->SetArg(scriptFileName,
                         fileNameList,
                         appName,
                         profile,
                         supportPath,
                         timeOut,
                         silent);
    connect(this, &MainWindow::workCanceled, threadObject, &Ui::Thread::Canceled);
    connect(threadObject, &Ui::Thread::progress, this, &MainWindow::SetValue);
    connect(threadObject, &Ui::Thread::finish, this, &MainWindow::AppStop);
    connect(threadObject, &Ui::Thread::reportItemResult, this, &MainWindow::ItemReport);
    connect(threadObject, &Ui::Thread::reportResult, this, &MainWindow::AppReport);

    threadObject->start();
    appRuning = true;

    QProgressDialog *pprd = new QProgressDialog();
    pprd->setModal(true);
    pprd->setWindowFlags(Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    pprd->setWindowTitle("Изменение DWG-файлов");
    pprd->setLabelText(text);
    pprd->setMinimum(0);
    pprd->setMinimumDuration(0);
    pprd->setAutoReset(false);
    pprd->setAutoClose(true);
    pprd->setPalette(plt);

    int countMax = fileNameList.size();
    pprd->setMaximum(countMax);
    while(count < countMax){
        QString textTmp = " " + QString::number(count + 1) + " из " + QString::number(countMax) + ":\n";
        QString fileName = fileNameList[count]; // current drawing file
        this->currentFile = fileName.toStdWString();
        pprd->setLabelText(text + textTmp + fileName);
        pprd->setValue(count);
        qApp->processEvents();
        if(pprd->wasCanceled()){
            pprd->setWindowTitle("Отмена задания");
            emit workCanceled();
            break;
        }
    }

    pprd->setValue(pprd->maximum());
    threadObject->wait();

    delete pprd;
    delete threadObject;
}

void MainWindow::StartAppInThread(QString app,
                                  QString paramsStr,
                                  int timeOut,
                                  bool silent){

}

bool MainWindow::CheckAndSetParams(map<wstring, wstring> paramsOld, map<wstring, wstring> paramsNew){

    bool result = false;
    map<wstring, wstring>::iterator it0, it1;
    for(it0 = paramsOld.begin(); it0 != paramsOld.end(); it0++){
        if(it0->first != L"path"){
            it1 = paramsNew.find(it0->first);
            if(it1 != paramsNew.end() && (it0->second != it1->second)){
                it0->second = it1->second;
                result = true;
            }
        }
    }
    this->paramsACD = paramsOld;

    return result;
}

bool MainWindow::CompareState(Ui::State state1, Ui::State state2){

    if((state1.boxAcad == state2.boxAcad) && (state1.boxProfile == state2.boxProfile)){
        return true;
    }
    else{
        return false;
    }
}

void MainWindow::SetValue(int value){

    count = value;
}

void MainWindow::AppStop(int value){

    if(appEnable){
        QStringList fileNameList = modelFileList->stringList();
        for(int i = 0; i < value; i++){
            fileNameList.pop_front();
        }
        SetModelFileList(fileNameList, false);
    }
}

void MainWindow::AppReport(int result){

    if(!appEnable){
        if(result == 0){
            appEnable = true;
        }
    }else{
        if(result != 0){
            ReportError();
        }
    }
    appRuning = false;
}

void MainWindow::ItemReport(int value){

    list<wstring> logLineList;
    logLineList.push_back(L"- " + currentFile);
    std::pair<std::wstring, bool> tmp;
    tmp.first = currentFile;
    if(value == 0){
        tmp.second = true;
        logLineList.push_back(L"-- Sucsess.");
    }else{
        tmp.second = false;
        logLineList.push_back(L"-- Error!");
    }
    reportList.push_back(tmp);
    Files::Files::UpdateTextFile(taskLogFile, logLineList);
}

void MainWindow::ReportError(){

    QString msg = "Ошибка обработки заданий:";

    for(size_t i = 0; i != reportList.size(); i++) {
        wstring str;
        std::pair<std::wstring, bool> tmp;
        tmp = reportList[i];
        if(tmp.second == false){
            str = L"\n" + tmp.first;
            msg = msg + QString::fromStdWString(str);
        }
    }

    QMessageBox *mBox = new QMessageBox(
                QMessageBox::Information,
                "Warning!",
                msg,
                QMessageBox::Ok
                );
    mBox->setDefaultButton(QMessageBox::Ok);

    mBox->exec();
    delete mBox;
}
