#ifndef THREAD_H
#define THREAD_H

#include <QThread>
#include <QString>
#include <QStringList>

namespace Ui {

    class Thread : public QThread
    {
    Q_OBJECT
    public:
        Thread();
        ~Thread();
        void SetArg(QString str,
                    QStringList fileNameListIn,
                    QString appNameIn,
                    QString profileIn,
                    QString supportPath,
                    int timeOut,
                    bool isSilent);

    protected:
        void run();

    signals:
        void progress(int);
        void finish(int);
        void reportResult(int);
        void reportItemResult(int);

    public slots:
        void Canceled();

    private:
        bool running;
        int count;
        QString scriptFileName;
        QStringList fileNameList;
        QString appName;
        QString profile;
        QString supportPath;
        int timeOut;
        bool isSilent;

    };

}

#endif // THREAD_H
