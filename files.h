#ifndef FILES_H
#define FILES_H

#include <list>
#include <map>

using namespace std;

namespace Files
{
    class Files{
    public:
        Files();
        ~Files();

        static list<wstring> GetFileLinesList(wstring fileName);
        static bool CreateTextFile(wstring fileName, list<wstring> lineList);
        static bool UpdateTextFile(wstring fileName, list<wstring> lineList);
        static bool DeleteFileNow(wstring fileName);
        static bool CheckDir(wstring dir);
        static bool RenameFile(wstring fileNameOld, wstring fileNameNew);
        static wstring ChangeSlash(wstring stringIn);
        static wstring AddQuotes(wstring stringIn);
        static wstring AddSlash(wstring stringIn);
        static list<wstring> SearchFiles(wstring path, wstring mask, bool useSubdirs);
        static wstring ChangeSubstr(wstring stringIn, wstring oldStr, wstring newStr);
        static list<wstring> GetListFromString(wstring stringIn, wstring sep);

    private:
        static wstring SplitUseSeparator(list<wstring> stringList, wstring sep);
        static list<wstring> GetFiles(wstring dir, bool useSubdirs);

    };
}

#endif // FILES_H
