#include <string>
#include <ctime>
#include <algorithm>
#include <windows.h>
#include "appsrv.h"

namespace App{

    AppSrv::AppSrv(){
    }

    AppSrv::~AppSrv(){
    }

    HANDLE AppSrv::StartApplication(wstring name, wstring params, bool hidden){

        HANDLE handle = NULL;
        int show = SW_HIDE; //
        if(!hidden){
            show = SW_SHOWNORMAL;
        }

        wstring strTmp = name + L" " + params;
        LPWSTR cmdStr = new TCHAR[strTmp.size() + 1];
        std::copy(strTmp.begin(), strTmp.end(), cmdStr);
        cmdStr[strTmp.size()] = 0;

        STARTUPINFO sti;
        ZeroMemory(&sti, sizeof(STARTUPINFO));
        PROCESS_INFORMATION pi;
        sti.dwFlags = STARTF_USESHOWWINDOW; // cti.wShowWindow не используется, если не установлен
        sti.wShowWindow = show;

        bool res = CreateProcess(
    //        bool res = CreateProcessA(
            NULL,           // No module name (use command line)
            cmdStr,        // Command line
            NULL,           // Process handle not inheritable
            NULL,           // Thread handle not inheritable
            FALSE,          // Set handle inheritance to FALSE
            0,              //0 - No creation flags
            NULL,           // Use parent's environment block
            NULL,           // Use parent's starting directory
            &sti,           // Pointer to STARTUPINFO structure
            &pi);           // Pointer to PROCESS_INFORMATION structure

        if(res){
            handle = pi.hProcess;
        }

        delete cmdStr;

        return handle;
    } // запуск приложения, возвращает указатель на запущенный процесс или NULL

    int AppSrv::WaitForAppFinished(HANDLE handle, int timeOut){
        int result = 0;
        (DWORD)timeOut; // время по истечении которого процесс убить принудительно
        DWORD res = WaitForSingleObject(handle, timeOut);
        if (res != WAIT_OBJECT_0){
            TerminateProcess(handle, NO_ERROR);
            result = 1;
        }

        return result;
    }

};
