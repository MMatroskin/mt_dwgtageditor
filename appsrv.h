#ifndef APPSRV_H
#define APPSRV_H

#include <vector>
#include <string>

using namespace std;

namespace App
{
    class AppSrv
    {
        public:
            AppSrv();
            ~AppSrv();
            static HANDLE StartApplication(wstring name, wstring params, bool hidden);
            static int WaitForAppFinished(HANDLE handle, int timeOut);
    };

}

#endif // APPSRV_H
