#ifndef PROFILESRV_H
#define PROFILESRV_H

#include <string>

using namespace std;

namespace Acad
{
    class ProfileSrv
    {
    public:
        ProfileSrv();
        ~ProfileSrv();

       static void DeleteFixedProfileBackup(wstring fileName);
       static bool CreateFixedProfileBackup(wstring fileName);
       static bool RestoreFixedProfile(wstring fileName);
       static bool ChangeFixedProfile(wstring fileName, wstring changeFileNameBase, wstring localeId);
       static bool CheckBackup(wstring fileName);
    };
}

#endif // PROFILESRV_H
