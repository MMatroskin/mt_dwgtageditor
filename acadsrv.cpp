#include "acadsrv.h"
#include <string>
#include <ctime>
#include <algorithm>
#include <windows.h>
#include "countwindows.h"

namespace Acad{

    AcadSrv::AcadSrv(){
    }

    AcadSrv::~AcadSrv(){
    }

    // список установленных Autocad с параметрами (параметры определены в классе AcadApp)
    vector<Acad::AcadApp> AcadSrv::GetInstalledAcad(){

        vector<Acad::AcadApp> result;
        wstring keyAcad = L"Software\\Autodesk\\AutoCAD";

        vector<wstring> subKeys = Reg::RegFunc::GetSubKeys(HKEY_CURRENT_USER, keyAcad);
        size_t subKeysSize = subKeys.size();
        for (size_t i = 0; i < subKeysSize; i++){
            wstring keyPath = keyAcad + L"\\" + subKeys[i];
            wstring keyValue = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, keyPath, L"CurVer");
            Acad::AcadApp *acd = new Acad::AcadApp(keyPath + L"\\" + keyValue);
            if(acd->GetProductName() != L""){
                result.push_back(*acd);
            }//

            delete acd;
        }
        std::reverse(std::begin(result), std::end(result)); // для отображения сначала более поздней версии ACAD

        return result;
    }

    wstring AcadSrv::GetAcadFullName(Acad::AcadApp *acd){
        wstring result = acd->GetAcadLocation() + L"\\acad.exe";
        return result;
    }

    void AcadSrv::HideAcadWindow(_PROCESS_INFORMATION *pid, int timeOut){

        DWORD id = pid->dwProcessId;
        CountWindows *counter = new CountWindows();
        vector<Acad::Proc> sourseProcessVec;

        //задержка, чтобы acad успел создать окно
        unsigned int startTime = clock();
        unsigned int currentTime;
        int count = 0;
        do{
            sourseProcessVec.clear();
            sourseProcessVec = counter->CountThem();
            size_t size = sourseProcessVec.size();
            for (size_t i = 0; i < size; i++){
                if(sourseProcessVec[i].id == id){
                    //processVec.push_back(sourseProcessVec[i]);
                    ShowWindow(sourseProcessVec[i].handle, SW_HIDE);
                }
            }
            count++;
            currentTime = clock() - startTime;
        }while (currentTime < timeOut); // timeOut sec wait for ACAD Window starting

        delete counter;
    }

    void AcadSrv::HideAcadWindow(wstring name, _PROCESS_INFORMATION *pid){
        wstring wndName(name.begin(), name.end());
        HWND handle = NULL;
        do{
            handle = FindWindow(0,wndName.c_str());
            DWORD dwPID = 0;
            GetWindowThreadProcessId(handle, &dwPID);
            if (dwPID == pid->dwProcessId){
                ShowWindow(handle, SW_HIDE);
            }
            else{
                handle = NULL;
            }
        }while (handle != NULL);
    }

    HANDLE AcadSrv::StartApplication(wstring name, wstring params, bool hidden){

        HANDLE handle = NULL;
        int show = SW_HIDE; //
        if(!hidden){
            show = SW_SHOWNORMAL;
        }

        wstring strTmp = name + L" " + params;
        LPWSTR cmdStr = new TCHAR[strTmp.size() + 1];
        std::copy(strTmp.begin(), strTmp.end(), cmdStr);
        cmdStr[strTmp.size()] = 0;

        STARTUPINFO sti;
        ZeroMemory(&sti, sizeof(STARTUPINFO));
        PROCESS_INFORMATION pi;
        sti.dwFlags = STARTF_USESHOWWINDOW; // cti.wShowWindow не используется, если не установлен
        sti.wShowWindow = show;

        bool res = CreateProcess(
//        bool res = CreateProcessA(
            NULL,           // No module name (use command line)
            cmdStr,        // Command line
            NULL,           // Process handle not inheritable
            NULL,           // Thread handle not inheritable
            FALSE,          // Set handle inheritance to FALSE
            0,              //0 - No creation flags
            NULL,           // Use parent's environment block
            NULL,           // Use parent's starting directory
            &sti,           // Pointer to STARTUPINFO structure
            &pi);           // Pointer to PROCESS_INFORMATION structure

        if(res){
            handle = pi.hProcess;
//            HideAcadWindow(&pi, 3000);
        }

        delete cmdStr;

        return handle;
    } // запуск приложения, возвращает указатель на запущенный процесс или NULL

    int AcadSrv::WaitForAppFinished(HANDLE handle, int timeOut){
        int result = 0;
        (DWORD)timeOut; // время по истечении которого процесс убить принудительно
        DWORD res = WaitForSingleObject(handle, timeOut);
        if (res != WAIT_OBJECT_0){
            TerminateProcess(handle, NO_ERROR);
            result = 1;
        }

        return result;
    }

}
