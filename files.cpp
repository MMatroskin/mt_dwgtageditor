#include "files.h"
#include <fstream>
#include <string>
#include <locale>
#include <Windows.h>
#include <cstdio>
#include <algorithm>

using namespace std;

namespace Files {

    Files::Files(){
    }

    Files::~Files(){
    }

    bool Files::CreateTextFile(wstring fileName, list<wstring> lineList){

        // http://rsdn.org/forum/cpp/4381954.hot
        // in MinGW ERROR : no matching function for call to 'std::basic_ofstream<wchar_t>::basic_ofstream(std::__cxx11::wstring)'
        // in :
        // wofstream file(fileName.toStdWString()); // cyr -> Wide
        // using QFile

        bool result = false;

        wofstream file(fileName); // cyr -> Wide
        if(file.out){
            file.imbue(std::locale("rus_rus.1251"));
            while (!lineList.empty()){
                wstring lineOut = lineList.front();
                lineList.pop_front();
                file << lineOut;
                file << endl;
            }
            file.close();
            result = true;
        }

        return result;
    }

    bool Files::UpdateTextFile(wstring fileName, list<wstring> lineList){

        bool result = false;

        wofstream file(fileName, ios_base::app);
        if(file.out){
            file.imbue(std::locale("rus_rus.1251"));
            while (!lineList.empty()){
                wstring lineOut = lineList.front();
                lineList.pop_front();
                file << lineOut;
                file << endl;
            }
            file.close();
            result = true;
        }

        return result;
    }

    list<wstring> Files::GetFileLinesList(wstring fileName){

        list<wstring> lineList;

        wifstream file(fileName);
        if(file){
            file.imbue(std::locale("rus_rus.1251"));
            wstring lineIn;
            while(!file.eof()){
                getline(file, lineIn);
                lineList.push_back(lineIn);
            }
            file.close();
        }
        return lineList; // in err -> empty list
    }

    bool Files::DeleteFileNow(wstring fileName){

        bool result = true;
//        if(remove(fileName.c_str()) != 0){
//            result = false;
//        }// c++17
        if(DeleteFileW(fileName.c_str()) == 0){
            result = false;
        }

        return result;
    }

    wstring Files::ChangeSlash(wstring stringIn){

        wstring sep = L"/";
        wstring newSep = L"\\";
        wstring result = L"";
        if(!stringIn.empty()){
            list<wstring> tmpList = Files::GetListFromString(stringIn, sep);
            result = Files::SplitUseSeparator(tmpList, newSep);
        }

        return result;
    }

    wstring Files::AddQuotes(wstring stringIn){

        wstring sep = L"\\";
        wstring result = L"";
        if(!stringIn.empty()){
            list<wstring> tmpList = Files::GetListFromString(stringIn, sep);
            result = tmpList.front();
            tmpList.pop_front();
            while(!tmpList.empty()){
                wstring tmp = tmpList.front();
                tmpList.pop_front();
                if(tmp.length() != 0){
                    result = result + sep + L"\"" + tmp + L"\"";
                }
            }
        }

        return result;
    }

    wstring Files::AddSlash(wstring stringIn){

        wstring sep = L"\\";
        wstring newSep = L"\\\\";
        wstring result = L"";
        if(!stringIn.empty()){
            list<wstring> tmpList = Files::GetListFromString(stringIn, sep);
            result = Files::SplitUseSeparator(tmpList, newSep);
        }

        return result;
    }

    wstring Files::SplitUseSeparator(list<wstring> stringList, wstring sep){

        wstring result = L"";
        if(!stringList.empty()){
            result = stringList.front();
            stringList.pop_front();
            while(!stringList.empty()){
                result = result + sep + stringList.front();
                stringList.pop_front();
            }
        }

        return result;
    }

    list<wstring> Files::GetListFromString(wstring stringIn, wstring sep){

        list<wstring> result;
        size_t pos = 0;
        size_t posNext;
        size_t delta = sep.length();
        if(!stringIn.empty()){
            while((posNext = stringIn.find(sep, pos)) != std::string::npos){
                wstring tmp = stringIn.substr(pos, posNext - pos);
                result.push_back(tmp);
                pos = posNext + delta;
            };
            result.push_back(stringIn.substr(pos));
        }

        return result;
    }

    list<wstring> Files::GetFiles(wstring dir, bool useSubdirs){

        list<wstring> result;
        list<wstring> tmp;
        wstring item = dir;
        wstring msk = L"*.*";
        wstring current = L"";
        HANDLE hand;
        WIN32_FIND_DATA dataFile;
        current = item + msk ;
        hand = FindFirstFile((current).c_str(), &dataFile);
        if (hand != INVALID_HANDLE_VALUE){
            do{
                if (lstrcmp(dataFile.cFileName, L".") == 0 || lstrcmp(dataFile.cFileName, L"..") == 0){
                    continue;
                }
    //            if (dataFile.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM){
    //                continue;
    //            }
                if (dataFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY && useSubdirs){
                    current = item + dataFile.cFileName + L"\\";
                    tmp = GetFiles(current, useSubdirs);
                    result.splice(result.end(), tmp);
                }else{
                    current = item + dataFile.cFileName;
                    result.push_back(current);
                }

            } while (FindNextFile(hand, &dataFile) != 0);
            FindClose(hand);
        }

        return result;
    }

    bool Files::CheckDir(wstring dir){

        bool result = false;
        wstring dirName = ChangeSlash(dir);
//        wstring tmp = &dirName.back();
//        while(tmp == L"\\"){
//            dirName.pop_back();
//            tmp = &dirName.back();
//        }
        HANDLE hand;
        WIN32_FIND_DATA dataFile;
        hand = FindFirstFile((dirName).c_str(), &dataFile);
        if(hand != INVALID_HANDLE_VALUE){
            if (dataFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
                result = true;
            }
        }
        return result;
    }

    bool Files::RenameFile(wstring fileNameOld, wstring fileNameNew){

        bool result = MoveFile(fileNameOld.c_str(), fileNameNew.c_str());
        return result;
    }

    list<wstring> Files::SearchFiles(wstring path, wstring mask, bool useSubdirs){

        list<wstring> result;
        list<wstring> tmp;
        wstring dir = path + L"\\";
        wstring strComp = mask;
        bool all = false;
        bool forName = false;
        bool forFullName = false;

        if(strComp == L"*.*"){
            all = true;
        }else if(strComp.find(L"*.") != std::string::npos){
            strComp = mask.substr(2);
        }else if(strComp.find(L".*") != std::string::npos){
            strComp = mask.substr(0, strComp.size() - 2);
            forName = true;
        }else{
            forFullName = true;
        }
        transform(strComp.begin(), strComp.end(), strComp.begin(), ::tolower);

        tmp = GetFiles(dir, useSubdirs);

        if(all){
            result = tmp;
        }else{
            list<wstring>::iterator it;
            for(it = tmp.begin(); it != tmp.end(); it++){
                wstring item = *it;
                size_t pos = item.find_last_of(L"\\");
                wstring str = item.substr(pos + 1);
                size_t len = str.length();
                if(!forFullName){
                    if(!forName){
                        str = str.substr(len - 3);
                    } else {
                        str = str.substr(0, len - 4);
                    }

                }
                transform(str.begin(), str.end(), str.begin(), ::tolower);
                if(str == strComp){
                    result.push_back(item);
                }
            }
        }

        return result;
    }


    wstring Files::ChangeSubstr(wstring stringIn, wstring oldStr, wstring newStr){

        wstring result = L"";
        if(!stringIn.empty()){
            list<wstring> tmpList = Files::GetListFromString(stringIn, oldStr);
            result = Files::SplitUseSeparator(tmpList, newStr);
        }

        return result;
    }

}
